<?php
	require_once(dirname(__FILE__).'/../../../../config/config.inc.php');
	if (!defined('_PS_VERSION_')) exit;
	require_once(dirname(__FILE__).'/../../libwebpay/healthcheck.php');

	$type = $_POST['TYPE'];

	switch($type)
	{
	  case 'checkInit':
	  
		$response = [];
	  
		$arg = [
			'MODO' 			=> $_POST['MODE'],
			'COMMERCE_CODE'	=> $_POST['COMMERCE_CODE'],
			'PUBLIC_CERT'   => $_POST['PUBLIC_CERT'],
			'PRIVATE_KEY'	=> $_POST['PRIVATE_KEY'],
			'PATPASS_CERT'	=> $_POST['PATPASS_CERT'],
			'PATPASS_MAIL' 	=> $_POST['PATPASS_MAIL'],
		    'PATPASS_UF' 	=> $_POST['PATPASS_UF'],
		    'PATPASS_DAY' 	=> $_POST['PATPASS_DAY'],
			'ECOMMERCE'     => 'prestashop'
		];
		
		$healthcheck = new HealthCheck($arg);
	  
		try
		{
			
			$response = $healthcheck->getInitTransaction();
			
			echo json_encode(['success' => true, 'msg' => json_decode($response)]);
		}
		catch (Exception $e)
		{
			echo json_encode(['success' => false, 'msg' => $e->getMessage()]);  
		}
	  
	  break;
	}	