<?php

include_once(dirname(__FILE__).'/../../../../config/config.inc.php');
include_once(dirname(__FILE__).'/../../../../init.php');
include_once(dirname(__FILE__).'/../../../../classes/Cookie.php');
require_once(dirname(__FILE__).'/../../libwebpay/patpass-config.php');
require_once(dirname(__FILE__).'/../../libwebpay/patpass-normal.php');

try{
	$config = array(
				"MODO"            => Configuration::get('PATPASS_AMBIENT'),             
				"PRIVATE_KEY"     => Configuration::get('PATPASS_SECRETCODE'),
				"PUBLIC_CERT"     => Configuration::get('PATPASS_CERTIFICATE'),
				"PATPASS_CERT"     => Configuration::get('PATPASS_CERTIFICATETRANSBANK'),            
				"COMMERCE_CODE" => Configuration::get('PATPASS_STOREID'),
				"PATPASS_MAIL" => Configuration::get('PATPASS_MAIL'),
				"PATPASS_UF" => Configuration::get('PATPASS_UF'),
				"PATPASS_DAY" => Configuration::get('PATPASS_DAY'),
				"URL_FINAL"       => Configuration::get('PATPASS_NOTIFYURL'),
				"URL_RETURN"      => Configuration::get('PATPASS_POSTBACKURL'),
				"ECOMMERCE"      => 'prestashop'
			);
	$pp_config = new PatPassConfig($config);
	$patpass = new PatPassNormal($pp_config);

	// Cart ID
	$cartid=Context::getContext()->cart->id;
	// Total de la Compra
	$total=Context::getContext()->cart->getOrderTotal(true, Cart::BOTH);

	// Obtención de serviceId (Feature en el Producto)
	$serviceId = '';
	$features = $cart->getProducts(false,false,null,true)[0][features];
	foreach ($features as &$feat){
		$featName = Feature::getFeature(1,$feat["id_feature"])["name"];
		$featIdValue = $feat["id_feature_value"];
		if(strcasecmp($featName, "serviceID") == 0){
			$featValues = FeatureValue::getFeatureValueLang($featIdValue);
			foreach ($featValues as &$val){
				if($val["value"] != null || $val["value"] != ""){
					$serviceId = $val["value"];
					break;
				}
			}
			break;
		}
	}
	if($serviceId==''){
		echo json_encode(array('error' => 1,'msg' => 'Error al Obtener Datos del Servicio o Producto, Favor Intentar Nuevamente...','token_ws' => '','url_ws' => ''));
		die;
	}

	// Obtención de subscriptionMonths (Feature en el Producto)
	$months = 0;
	$expirationDate = '';
	$features = $cart->getProducts(false,false,null,true)[0][features];
	foreach ($features as &$feat){
		$featName = Feature::getFeature(1,$feat["id_feature"])["name"];
		$featIdValue = $feat["id_feature_value"];
		if(strcasecmp($featName, "subscriptionMonths") == 0){
			$featValues = FeatureValue::getFeatureValueLang($featIdValue);
			foreach ($featValues as &$val){
				if($val["value"] != null || $val["value"] != ""){
					$months = $val["value"];
					break;
				}
			}
			break;
		}
	}
	if($months==0){
		echo json_encode(array('error' => 1,'msg' => 'Error al Obtener Datos del Servicio o Producto, Favor Intentar Nuevamente...','token_ws' => '','url_ws' => ''));
		die;
	}
	else{
		// Calculo Expiration Date
		if($config['PATPASS_DAY']==0){
			$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, date("d"), date("Y")));
		}
		elseif($config['PATPASS_DAY']==99){
			$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, date("t",mktime(0, 0, 0, date("m")+$months, 1, date("Y"))), date("Y")));  
		}
		else{
			$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, $config['PATPASS_DAY'], date("Y")));  
		}
	}

	$result = $patpass->initTransaction($total, $sessionId="123abc", $cartid, $config['URL_RETURN'], $serviceId, $_POST['rut'], $_POST['firstname'], $_POST['lastname1'], $_POST['lastname2'], $_POST['email'], $_POST['tel'], $expirationDate);

	if($result['token_ws']==null || $result['url']==null){
		echo json_encode(array('error' => 1,'msg' => 'Error al Comunicarse con el Servicio de PatPass','token_ws' => '','url_ws' => ''));
		die;
	}

	echo json_encode(array('error' => 0,'msg' => 'Everything is OK','token_ws' => $result['token_ws'],'url_ws' => $result['url']));
	die;
}catch(Exception $e){
	echo json_encode(array('error' => 1,'msg' => 'Error al Comunicarse con el Servicio de PatPass','token_ws' => '','url_ws' => ''));
	die;
}