<?php
require_once(dirname(__FILE__).'/../../libwebpay/patpass-config.php');
require_once(dirname(__FILE__).'/../../libwebpay/patpass-normal.php');

class PatPassPaymentModuleFrontController extends ModuleFrontController {

	public $ssl = true;
	public $display_column_left = false;

	public function initContent(){

		$PatPassPayment = new PatPass();
		$cart = $this->context->cart;
		$cartId = $this->context->cart->id;
		parent::initContent();
		$order = new Order(Order::getOrderByCartId($cartId));
		$customer = new Customer((int)$cart->id_customer);
		$validation = 0;
		$error_msg = 'Everything is OK';

		// Validacion Cantidad de Servicios en Carro
		if($cart->nbProducts()>1){
			$validation = 2;
			$error_msg = 'Solo se permite 1 Servicio PatPass en el carro de compras.';
		}
		else{
			// Obtención de serviceId (Feature en el Producto)
			$serviceId = '';
			$features = $cart->getProducts(false,false,null,true)[0][features];
			foreach ($features as &$feat){
				$featName = Feature::getFeature(1,$feat["id_feature"])["name"];
				$featIdValue = $feat["id_feature_value"];
				if(strcasecmp($featName, "serviceID") == 0){
					$featValues = FeatureValue::getFeatureValueLang($featIdValue);
					foreach ($featValues as &$val){
						if($val["value"] != null || $val["value"] != ""){
							$serviceId = $val["value"];
							break;
						}
					}
					break;
				}
			}
			if($serviceId==''){
				$validation = 1;
				$error_msg = 'Servicio no compatible con PatPass, favor contactar al comercio.';
			}
			else{
				// Obtención de subscriptionMonths (Feature en el Producto)
				$months = 0;
				$expirationDate = '';
				$features = $cart->getProducts(false,false,null,true)[0][features];
				foreach ($features as &$feat){
					$featName = Feature::getFeature(1,$feat["id_feature"])["name"];
					$featIdValue = $feat["id_feature_value"];
					if(strcasecmp($featName, "subscriptionMonths") == 0){
						$featValues = FeatureValue::getFeatureValueLang($featIdValue);
						foreach ($featValues as &$val){
							if($val["value"] != null || $val["value"] != ""){
								$months = $val["value"];
								break;
							}
						}
						break;
					}
				}
				if($months==0){
					$validation = 1;
					$error_msg = 'Servicio no compatible con PatPass, favor contactar al comercio.';
				}
				else{
					// Calculo Expiration Date
					if($config['PATPASS_DAY']==0){
						$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, date("d"), date("Y")));
					}
					elseif($config['PATPASS_DAY']==99){
						$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, date("t",mktime(0, 0, 0, date("m")+$months, 1, date("Y"))), date("Y")));  
					}
					else{
						$expirationDate = date('Y-m-d',mktime(0, 0, 0, date("m")+$months, $config['PATPASS_DAY'], date("Y")));  
					}
				}
			}
		}

		Context::getContext()->smarty->assign(array(
			'validation' => $validation,
			'errmsg' => $error_msg,
			'nbProducts' => $cart->nbProducts(),
			'cust_currency' => $cart->id_currency,
			'currencies' => $this->module->getCurrency((int)$cart->id_currency),
			'total' => $cart->getOrderTotal(true, Cart::BOTH),
			'name' => $customer->firstname,
			'lastname' => $customer->lastname,
			'mail' => $customer->email,
			'this_path' => $this->module->getPathUri(),
			'this_path_bw' => $this->module->getPathUri(),
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/',
			'url_js' => _PS_JS_DIR_.'jquery/jquery-'._PS_JQUERY_VERSION_.'.min.js'
		));

		$url_base = Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . "index.php?fc=module&module={$PatPassPayment->name}&controller=validate&cartId=" . $cartId;
		$url_exito   = $url_base."&return=ok";
		$url_fracaso = $url_base."&return=error";
		$url_confirmacion = Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__ . "modules/{$PatPassPayment->name}/validate.php";

		Configuration::updateValue('PATPASS_URL_FRACASO', $url_fracaso);
		Configuration::updateValue('PATPASS_URL_EXITO', $url_exito);
		Configuration::updateValue('PATPASS_URL_CONFIRMACION', $url_confirmacion);

		$this->setTemplate('module:patpass/views/templates/front/payment.tpl');
	}
}