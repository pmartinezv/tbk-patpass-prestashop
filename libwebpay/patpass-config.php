<?php
# @Author: Rolando Demanet C
# @Date:   2018-05-07T16:51:03-04:00
# @Email:  rdemanet@allware.cl
# @Filename: patpass-config.php
# @Last modified by:   Rolando Demanet C
# @Last modified time: 2018-05-07T16:51:03-04:00


class PatPassConfig{
        private $params = array();

        function __construct($params){
                $this->params = $params;
        }

        public function getParams(){
        return $this->params;
    }

        public function getParam($name){
        return $this->params[$name];
    }

        public function getModo(){
                $modo = $this->params["MODO"];
        if (!isset($modo) || $modo == ""){
            $modo = "INTEGRACION";
        }
                return $modo;
        }
}


?>
