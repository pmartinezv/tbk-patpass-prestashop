<?php
use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_'))
  exit;

require_once('libwebpay/healthcheck.php');
require_once('libwebpay/loghandler.php');

class PatPass extends PaymentModule {

  protected $_errors = array();
  var $healthcheck;
  var $log;

  public function __construct() {
    $this->name = 'patpass';
    $this->tab = 'payments_gateways';
    $this->version = '1.0.0';
    $this->author = 'Transbank';
    $this->need_instance = 1;
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = 'PatPass by Webpay';
    $this->description = 'Recibe pagos en linea con Tarjetas de Credito en tu Prestashop a traves de PatPass by Webpay';
    $this->tab = 'payments_gateways';
    $this->controllers = array('payment', 'validate');

    Context::getContext()->cookie->__set('PATPASS_TITLE', "Pago con PatPass");
    Context::getContext()->cookie->__set('PATPASS_BUTTON_TITLE', "Pago electronico con Tarjetas de Credito a traves de PatPass by Webpay");

    $this->loadIntegrationCertificates();

    $this->pluginValidation();
    $this->loadPluginConfiguration();
    $arg =  array('MODO' => $this->ambient,
      'COMMERCE_CODE' => $this->storeID,
      'PUBLIC_CERT' => $this->certificate,
      'PRIVATE_KEY' => $this->secretCode,
      'PATPASS_CERT' => $this->certificateTransbank,
      'PATPASS_MAIL' => $this->commerceMail,
      'PATPASS_UF' => $this->ufFlag,
      'PATPASS_DAY' => $this->expirationDay,
      'ECOMMERCE' => 'prestashop');
    //$this->healthcheck = new HealthCheck($arg);
    //$this->datos_hc = json_decode($this->healthcheck->printFullResume());
    $this->log = new loghandler($arg['ECOMMERCE']);

  }

  public function install() {
    $this->setupPlugin();
    return parent::install() &&
    $this->registerHook('header') &&
    $this->registerHook('paymentOptions') &&
    $this->registerHook('paymentReturn') &&
    $this->registerHook('displayPayment') &&
    $this->registerHook('displayPaymentReturn');
  }

  public function uninstall() {
    if (!parent::uninstall() || !Configuration::deleteByName("PATPASS"))
      return false;


    return true;
  }

  public function hookPaymentReturn($params) {

    if (!$this->active)
      return;

    $state = $params['order']->getCurrentState();

    $this->smarty->assign(array(
     'shop_name' => $this->context->shop->name,
     'total_to_pay' =>  $params['order']->getOrdersTotalPaid(),
     'status' => 'ok',
     'id_order' => $params['order']->id,
     'PATPASS_RESULT_DESC' => Context::getContext()->cookie->PATPASS_RESULT_DESC,
     'PATPASS_VOUCHER_NROTARJETA' => Context::getContext()->cookie->PATPASS_VOUCHER_NROTARJETA,
     'PATPASS_VOUCHER_TXDATE_FECHA' => Context::getContext()->cookie->PATPASS_VOUCHER_TXDATE_FECHA,
     'PATPASS_VOUCHER_TXDATE_HORA' => Context::getContext()->cookie->PATPASS_VOUCHER_TXDATE_HORA,
     'PATPASS_VOUCHER_TOTALPAGO' => Context::getContext()->cookie->PATPASS_VOUCHER_TOTALPAGO,
     'PATPASS_VOUCHER_ORDENCOMPRA' => Context::getContext()->cookie->PATPASS_VOUCHER_ORDENCOMPRA,
     'PATPASS_VOUCHER_AUTCODE' => Context::getContext()->cookie->PATPASS_VOUCHER_AUTCODE,
     'PATPASS_VOUCHER_TIPOCUOTAS' => Context::getContext()->cookie->PATPASS_VOUCHER_TIPOCUOTAS,
     'PATPASS_VOUCHER_TIPOPAGO' => Context::getContext()->cookie->PATPASS_VOUCHER_TIPOPAGO,
     'PATPASS_VOUCHER_NROCUOTAS' => Context::getContext()->cookie->PATPASS_VOUCHER_NROCUOTAS,
     'PATPASS_RESULT_CODE' => Context::getContext()->cookie->PATPASS_RESULT_CODE,
     'PATPASS_TX_ANULADA' => Context::getContext()->cookie->PATPASS_TX_ANULADA

   ));
    if (isset($params['order']->reference) && !empty($params['order']->reference))
      $this->smarty->assign('reference', $params['order']->reference);


    return $this->display(__FILE__, 'views/templates/hook/payment_return.tpl');

  }
  public function hookPaymentOptions($params)
  {

    if (!$this->active) {
      return;
    }
    if (!$this->checkCurrency($params['cart'])) {
      return;
    }

    $payment_options = [
      $this->getWPPaymentOption()

    ];

    return $payment_options;
  }


  public function checkCurrency($cart)
  {

    $currency_order = new Currency($cart->id_currency);
    $currencies_module = $this->getCurrency($cart->id_currency);

    if (is_array($currencies_module)) {
      foreach ($currencies_module as $currency_module) {
        if ($currency_order->id == $currency_module['id_currency']) {
          return true;
        }
      }
    }
    return false;
  }

  public function getWPPaymentOption()
  {


   $WPOption = new PaymentOption();

   $paymentController = $this->context->link->getModuleLink(
    $this->name,'payment',array(),true);
   $WPOption->setCallToActionText($this->l('Pago con PatPass'))
   ->setAction($paymentController);

   return $WPOption;
 }



 public function getContent() {

  $activeShopID = (int)Context::getContext()->shop->id;
  $shopDomainSsl = Tools::getShopDomainSsl(true, true);
  $change=false;

  if (Tools::getIsset('patpass_updateSettings')) {

    if (Tools::getValue('ambient') !=  Configuration::get('PATPASS_AMBIENT'))
     $change=true;


   Configuration::updateValue('PATPASS_STOREID', trim(Tools::getValue('storeID')));
   Configuration::updateValue('PATPASS_SECRETCODE', trim(Tools::getValue('secretCode')));
   Configuration::updateValue('PATPASS_CERTIFICATE', Tools::getValue('certificate'));
   Configuration::updateValue('PATPASS_CERTIFICATETRANSBANK', Tools::getValue('certificateTransbank'));
   Configuration::updateValue('PATPASS_AMBIENT', Tools::getValue('ambient'));
   Configuration::updateValue('PATPASS_MAIL', Tools::getValue('commerceMail'));
   Configuration::updateValue('PATPASS_UF', Tools::getValue('ufFlag'));
   Configuration::updateValue('PATPASS_DAY', Tools::getValue('expirationDay'));
   Configuration::updateValue('PATPASS_NOTIFYURL', Context::getContext()->link->getModuleLink($this->name, 'validate', array(), true));
   Configuration::updateValue('PATPASS_POSTBACKURL', Context::getContext()->link->getModuleLink($this->name, 'validate', array(), true));

   $this->loadPluginConfiguration();
   $this->pluginValidation();

 }else{
  $this->loadPluginConfiguration();
}
$arg =  array('MODO' => $this->ambient,
  'COMMERCE_CODE' => $this->storeID,
  'PUBLIC_CERT' => $this->certificate,
  'PRIVATE_KEY' => $this->secretCode,
  'PATPASS_CERT' => $this->certificateTransbank,
  'PATPASS_MAIL' => $this->commerceMail,
  'PATPASS_UF' => $this->ufFlag,
  'PATPASS_DAY' => $this->expirationDay,
  'ECOMMERCE' => 'prestashop');

try{
  $this->healthcheck = new HealthCheck($arg);
  if ($change){
    $rs = $this->healthcheck->getpostinstallinfo();
  }
  $this->datos_hc = json_decode($this->healthcheck->printFullResume());
}
catch(Exception $e){
  return $this->display($this->name, 'views/templates/admin/config.tpl');
}

Context::getContext()->smarty->assign(
  array(
    'errors' => $this->_errors,
    'post_url' => $_SERVER['REQUEST_URI'],
    'data_storeid_init' => $this->storeID_init,
    'data_secretcode_init' => $this->secretCode_init,
    'data_certificate_init' => $this->certificate_init,
    'data_certificatetransbank_init' => $this->certificateTransbank_init,
    'data_commercemail_init' => $this->commerceMail_init,
    'data_ufflag_init' => $this->ufFlag_init,
    'data_expirationday_init' => $this->expirationDay_init,
    'data_storeid' => $this->storeID,
    'data_secretcode' => $this->secretCode,
    'data_certificate' => $this->certificate,
    'data_certificatetransbank' => $this->certificateTransbank,
    'data_ambient' => $this->ambient,
    'data_commercemail' => $this->commerceMail,
    'data_ufflag' => $this->ufFlag,
    'data_expirationday' => $this->expirationDay,
    'data_title' => $this->title,
    'version' => $this->version,
    'api_version' => '1.0',
    'img_icono' => "https://www.transbank.cl/public/img/LogoWebpay.png",
    'patpass_notify_url' => $shopDomainSsl . __PS_BASE_URI__ . "modules/{$this->name}/controllers/front/validate.php",
    'patpass_postback_url' => $shopDomainSsl . __PS_BASE_URI__ . "modules/{$this->name}/controllers/front/validate.php",
    'cert_vs_private' =>$this->datos_hc->validate_certificates->consistency->cert_vs_private_key,
    'commerce_code_validate' =>$this->datos_hc->validate_certificates->consistency->commerce_code_validate,
    'subject_commerce_code' =>$this->datos_hc->validate_certificates->cert_info->subject_commerce_code,
    'cert_version' =>$this->datos_hc->validate_certificates->cert_info->version,
    'cert_is_valid' =>$this->datos_hc->validate_certificates->cert_info->is_valid,
    'valid_from' =>$this->datos_hc->validate_certificates->cert_info->valid_from,
    'valid_to' =>$this->datos_hc->validate_certificates->cert_info->valid_to,
    'init_status' =>$this->datos_hc->validate_init_transaction->status->string,
    'init_error_error' => (isset($this->datos_hc->validate_init_transaction->response->error)) ? $this->datos_hc->validate_init_transaction->response->error : NULL,
    'init_error_detail' => (isset($this->datos_hc->validate_init_transaction->response->detail)) ? $this->datos_hc->validate_init_transaction->response->detail : NULL,
    'init_success_url' =>$this->datos_hc->validate_init_transaction->response->url,
    'init_success_token' =>$this->datos_hc->validate_init_transaction->response->token_ws,
    'php_status' =>$this->datos_hc->server_resume->php_version->status,
    'php_version' =>$this->datos_hc->server_resume->php_version->version,
    'server_version' =>$this->datos_hc->server_resume->server_version->server_software,
    'ecommerce' =>$this->datos_hc->server_resume->plugin_info->ecommerce,
    'ecommerce_version' =>$this->datos_hc->server_resume->plugin_info->ecommerce_version,
    'current_plugin_version' =>$this->datos_hc->server_resume->plugin_info->current_plugin_version,
    'last_plugin_version' =>$this->datos_hc->server_resume->plugin_info->last_plugin_version,
    'openssl_status' =>$this->datos_hc->php_extensions_status->openssl->status,
    'openssl_version' =>$this->datos_hc->php_extensions_status->openssl->version,
    'SimpleXML_status' =>$this->datos_hc->php_extensions_status->SimpleXML->status,
    'SimpleXML_version' =>$this->datos_hc->php_extensions_status->SimpleXML->version,
    'soap_status' =>$this->datos_hc->php_extensions_status->soap->status,
    'soap_version' =>$this->datos_hc->php_extensions_status->soap->version,
    'mcrypt_status' =>$this->datos_hc->php_extensions_status->mcrypt->status,
    'mcrypt_version' =>$this->datos_hc->php_extensions_status->mcrypt->version,
    'dom_status' =>$this->datos_hc->php_extensions_status->dom->status,
    'dom_version' =>$this->datos_hc->php_extensions_status->dom->version,
    'php_info' =>$this->datos_hc->php_info->string->content,
    'lockfile' => json_decode($this->log->getLockFile(),true)['status'],
    'logs' => (isset( json_decode($this->log->getLastLog(),true)['log_content'])) ?  json_decode($this->log->getLastLog(),true)['log_content'] : NULL,
    'log_file' => (isset( json_decode($this->log->getLastLog(),true)['log_file'])) ?  json_decode($this->log->getLastLog(),true)['log_file'] : NULL,
    'log_weight' => (isset( json_decode($this->log->getLastLog(),true)['log_weight'])) ?  json_decode($this->log->getLastLog(),true)['log_weight'] : NULL,
    'log_regs_lines' => (isset( json_decode($this->log->getLastLog(),true)['log_regs_lines'])) ?  json_decode($this->log->getLastLog(),true)['log_regs_lines'] : NULL,
    'log_days' => $this->log->getValidateLockFile()['max_logs_days'],
    'log_size' => $this->log->getValidateLockFile()['max_log_weight'],
    'log_dir' => json_decode($this->log->getResume(),true)['log_dir'],
    'logs_count' => json_decode($this->log->getResume(),true)['logs_count']['log_count'],
    'logs_list' => json_decode($this->log->getResume(),true)['logs_list'],
    'url_js' => _PS_JS_DIR_.'jquery/jquery-'._PS_JQUERY_VERSION_.'.min.js',
    'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__."modules/{$this->name}/",
  )
);

return $this->display($this->name, 'views/templates/admin/config.tpl');

}


private function pluginValidation() {
  $this->_errors = array();
}

private function adminValidation() {
  $this->_errors = array();

}

private function loadPluginConfiguration() {
  $this->storeID = Configuration::get('PATPASS_STOREID');
  $this->secretCode = Configuration::get('PATPASS_SECRETCODE');
  $this->certificate = Configuration::get('PATPASS_CERTIFICATE');
  $this->certificateTransbank = Configuration::get('PATPASS_CERTIFICATETRANSBANK');
  $this->ambient = Configuration::get('PATPASS_AMBIENT');
  $this->commerceMail = Configuration::get('PATPASS_MAIL');
  $this->ufFlag = Configuration::get('PATPASS_UF');
  $this->expirationDay = Configuration::get('PATPASS_DAY');
  $this->title = Context::getContext()->cookie->PATPASS_TITLE;
  $this->patpass_notify_url = Configuration::get('PATPASS_NOTIFYURL');
  $this->patpass_postback_url = Configuration::get('PATPASS_POSTBACKURL');
}


private function setupPlugin() {

 $this->loadIntegrationCertificates();

 Configuration::updateValue('PATPASS_STOREID', $this->storeID_init);
 Configuration::updateValue('PATPASS_SECRETCODE', str_replace("<br/>", "\n", $this->secretCode_init));
 Configuration::updateValue('PATPASS_CERTIFICATE', str_replace("<br/>", "\n", $this->certificate_init));
 Configuration::updateValue('PATPASS_CERTIFICATETRANSBANK', str_replace("<br/>", "\n", $this->certificateTransbank_init));
 Configuration::updateValue('PATPASS_AMBIENT', "INTEGRACION");
 Configuration::updateValue('PATPASS_NOTIFYURL', Context::getContext()->link->getModuleLink($this->name, 'validate', array(), true));
 Configuration::updateValue('PATPASS_POSTBACKURL', Context::getContext()->link->getModuleLink($this->name, 'validate', array(), true));
 Configuration::updateValue('PATPASS_MAIL', $this->commerceMail);
 Configuration::updateValue('PATPASS_UF', $this->ufFlag);
 Configuration::updateValue('PATPASS_DAY', $this->expirationDay);
}


private function loadIntegrationCertificates() {
  $this->storeID_init = "597020000548";

  $this->secretCode_init = "-----BEGIN RSA PRIVATE KEY-----"
  . "<br/>MIIEpQIBAAKCAQEA0t4cnHnAr/05KYwk+2ziYKUAxk/E0LhpG8KSyJp9Kl1G09I8"
  . "<br/>xBhSnF23XpEm6grgKmxzbGtj8t8u9P54sNxNrcs0tawV0qHfzsS63hAJ1254LeZA"
  . "<br/>uTVpWXUnEl4DWI9Pq67OWUovUPL/mQzcZBJShspC6zX0unfWRnoDeNtx07aj/xq0"
  . "<br/>PXXtqOEJhyY6EyRsO9XWPVKIm5ZnVCBg9mtrZZkwwROETlD65meCE6gYC6WFrB5F"
  . "<br/>UvjS72CnA0ZynFj/Vb4Ymi6Ot5M3f0NmGG9q0kweOQfNyv+e/QoB7KVHLrpMdcKO"
  . "<br/>70D+ItPK4wdUlOfbTZJYWUaK6F/z2LkF0APD0QIDAQABAoIBAFrSRZpzqjViqHMn"
  . "<br/>pGoSLLKZfurrQobvVn4ZYOU7/Pr5L99d5sRDAZnNl4QImq0lQAWlrlUdL/BUhkIJ"
  . "<br/>NGxghqh7JFm3I7MT+3RwMVghqkt6jhKe4HOk+JoKJmj3yxMirprwcHnuxNBlyQbf"
  . "<br/>jjEf3yGlDguGssB5ivXR6ZrtUWpwsK9OBiRctSeceFE4r68ldDKRFVLNURkK1AEd"
  . "<br/>lCbhWp1a8W5CKvPjj6Rpq9r1kmTJA1h9NSkLum4f7aewvFhG6bHWEcjA6EWieipj"
  . "<br/>2zk74/1HBDpujHJSUzTcXbiIsOVtY2Qh+74CMzHUUS41dGQNk/l/qUk7tRljwjFm"
  . "<br/>PerH1FECgYEA6IKDDTbaLNsK4BefqlXZvCXbm0irxCb3XvvcsTqcDyZG0KWOCoXw"
  . "<br/>B24xrsMfbheR0L6x4JUS6RM4bvTEhK4gJzDDeFhfPj2Kuav9nl1oEOZaJ5rxLlz7"
  . "<br/>90g/f6kqjW3qlBVSFtz/jVrAk+lju9piHudwerjLA7TXLLm0tJZf7N0CgYEA6Cvc"
  . "<br/>HzDhxdNYVakWsTBKI/uOUwt7cbHUr7OfoQA5dTxqHvFKBjrY3rgcI3/4vU3cEwRm"
  . "<br/>a4X6RRSljyKAel54h4eJndP9+dB3VrOOkvuHXOLEVwwzfdaO+4FRQXR+0i9T46RK"
  . "<br/>7FJy6XLfhPwZozebXbSTg/WZJ5UczVmxBYzquYUCgYEAu1t/0wQiZwbTCqS+qnmn"
  . "<br/>jK9M+SJkFxn3N/joa3/5BVQouDTP8rbfJn2rV1IwX3xqqbUgjQJTTLGKRg7C1M+j"
  . "<br/>ZTEsMiu0A+l/ggKPyi8mjoewmj2Gn3+aIjd7w5lDitfJsS5FCdtnqjY4/HeTQGrH"
  . "<br/>qnOA9cM3BHOS+J1keii6f5kCgYEAofbO9bdtGUuJySBPY0azwgxgSlCtSjBrljLx"
  . "<br/>vihg7Qc7ZOCg6l2tIxo/DwjcZntldqLQLFxnrj9sC8Fe7X7wCGQmPcNA30BtsD9M"
  . "<br/>y/7KfKL5o1wwo63FS3D4VXhGbKx1kk3vspMF9ROdGLGh1Poa2bD6Y8k2kaV1VVAn"
  . "<br/>rR6UNN0CgYEAgWw0NRz9X3V5K0WfrXnqBEvDiJ9MnsO3Y0SmV+zbTVYQ2zf2iYT+"
  . "<br/>Wp4Tpjfc2k8s1dU33YnsrLaecB3gEFeEHJdN9qOPkk/yElVrcCKfFEUmPu7wvHZI"
  . "<br/>yCLqC4NXGoXossGlQ2XPCSYacEQ5JJTtv+sBufhJZTwuqEBSGS7PurA="
  . "<br/>-----END RSA PRIVATE KEY-----";

  $this->certificate_init = "-----BEGIN CERTIFICATE-----"
  . "<br/>MIIDujCCAqICCQDHWKiW6dFYqjANBgkqhkiG9w0BAQsFADCBnjELMAkGA1UEBhMC"
  . "<br/>Q0wxETAPBgNVBAgMCFNhbnRpYWdvMRIwEAYDVQQKDAlUcmFuc2JhbmsxETAPBgNV"
  . "<br/>BAcMCFNhbnRpYWdvMRUwEwYDVQQDDAw1OTcwMjAwMDA1NDgxFzAVBgNVBAsMDkNh"
  . "<br/>bmFsZXNSZW1vdG9zMSUwIwYJKoZIhvcNAQkBFhZpbnRlZ3JhZG9yZXNAdmFyaW9z"
  . "<br/>LmNsMB4XDTE2MDYyODIwMDQ0N1oXDTI0MDYyNjIwMDQ0N1owgZ4xCzAJBgNVBAYT"
  . "<br/>AkNMMREwDwYDVQQIDAhTYW50aWFnbzESMBAGA1UECgwJVHJhbnNiYW5rMREwDwYD"
  . "<br/>VQQHDAhTYW50aWFnbzEVMBMGA1UEAwwMNTk3MDIwMDAwNTQ4MRcwFQYDVQQLDA5D"
  . "<br/>YW5hbGVzUmVtb3RvczElMCMGCSqGSIb3DQEJARYWaW50ZWdyYWRvcmVzQHZhcmlv"
  . "<br/>cy5jbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANLeHJx5wK/9OSmM"
  . "<br/>JPts4mClAMZPxNC4aRvCksiafSpdRtPSPMQYUpxdt16RJuoK4Cpsc2xrY/LfLvT+"
  . "<br/>eLDcTa3LNLWsFdKh387Eut4QCddueC3mQLk1aVl1JxJeA1iPT6uuzllKL1Dy/5kM"
  . "<br/>3GQSUobKQus19Lp31kZ6A3jbcdO2o/8atD117ajhCYcmOhMkbDvV1j1SiJuWZ1Qg"
  . "<br/>YPZra2WZMMEThE5Q+uZnghOoGAulhaweRVL40u9gpwNGcpxY/1W+GJoujreTN39D"
  . "<br/>ZhhvatJMHjkHzcr/nv0KAeylRy66THXCju9A/iLTyuMHVJTn202SWFlGiuhf89i5"
  . "<br/>BdADw9ECAwEAATANBgkqhkiG9w0BAQsFAAOCAQEARz2BbG8QGKqjUYorkCe+eaE5"
  . "<br/>WnZN5q8/ihisJtsa9yXO7DUe8912094Wd/ZqwYQ1eBr0vEdwQ1Mit2lkBOkyNqR3"
  . "<br/>af3/8znCxiTqvJdo4r3sp6nZV2m6zjicQwJ3aWFP8mqeTsyG3rMZBin0QaDoYCM+"
  . "<br/>5qVZhQBycggsPMPnZ3fvBIslWCd6JBPYZ4agXNLdAsTmxYxjpuyOM+qTN9hdYdzb"
  . "<br/>jaJ/IVa8NZrrSZU6BxooybSHNJ5+x0dc9Q/6A7txTnjTj8Iy9gfjokXRgpADpFq9"
  . "<br/>mnf2hxewHnOGRcfAHBgS6vrpuAJ7/yIMewcMtu09ukFY7/d23CDPGVisDdDJwA=="
  . "<br/>-----END CERTIFICATE-----";

  $this->certificateTransbank_init = "-----BEGIN CERTIFICATE-----"
  . "<br/>MIIDKTCCAhECBFZl7uIwDQYJKoZIhvcNAQEFBQAwWTELMAkGA1UEBhMCQ0wxDjAMBgNVBAgMBUNo"
  . "<br/>aWxlMREwDwYDVQQHDAhTYW50aWFnbzEMMAoGA1UECgwDa2R1MQwwCgYDVQQLDANrZHUxCzAJBgNV"
  . "<br/>BAMMAjEwMB4XDTE1MTIwNzIwNDEwNloXDTE4MDkwMjIwNDEwNlowWTELMAkGA1UEBhMCQ0wxDjAM"
  . "<br/>BgNVBAgMBUNoaWxlMREwDwYDVQQHDAhTYW50aWFnbzEMMAoGA1UECgwDa2R1MQwwCgYDVQQLDANr"
  . "<br/>ZHUxCzAJBgNVBAMMAjEwMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAizJUWTDC7nfP"
  . "<br/>3jmZpWXFdG9oKyBrU0Bdl6fKif9a1GrwevThsU5Dq3wiRfYvomStNjFDYFXOs9pRIxqX2AWDybjA"
  . "<br/>X/+bdDTVbM+xXllA9stJY8s7hxAvwwO7IEuOmYDpmLKP7J+4KkNH7yxsKZyLL9trG3iSjV6Y6SO5"
  . "<br/>EEhUsdxoJFAow/h7qizJW0kOaWRcljf7kpqJAL3AadIuqV+hlf+Ts/64aMsfSJJA6xdbdp9ddgVF"
  . "<br/>oqUl1M8vpmd4glxlSrYmEkbYwdI9uF2d6bAeaneBPJFZr6KQqlbbrVyeJZqmMlEPy0qPco1TIxrd"
  . "<br/>EHlXgIFJLyyMRAyjX9i4l70xjwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBn3tUPS6e2USgMrPKp"
  . "<br/>sxU4OTfW64+mfD6QrVeBOh81f6aGHa67sMJn8FE/cG6jrUmX/FP1/Cpbpvkm5UUlFKpgaFfHv+Kg"
  . "<br/>CpEvgcRIv/OeIi6Jbuu3NrPdGPwzYkzlOQnmgio5RGb6GSs+OQ0mUWZ9J1+YtdZc+xTga0x7nsCT"
  . "<br/>5xNcUXsZKhyjoKhXtxJm3eyB3ysLNyuL/RHy/EyNEWiUhvt1SIePnW+Y4/cjQWYwNqSqMzTSW9TP"
  . "<br/>2QR2bX/W2H6ktRcLsgBK9mq7lE36p3q6c9DtZJE+xfA4NGCYWM9hd8pbusnoNO7AFxJZOuuvLZI7"
  . "<br/>JvD7YLhPvCYKry7N6x3l"
  . "<br/>-----END CERTIFICATE-----";

  $this->commerceMail_init = "user@domain.com";
  $this->ufFlag_init = "false";
  $this->expirationDay_init = 0;

  $this->ambient = Configuration::get('PATPASS_AMBIENT');
  $this->title = Context::getContext()->cookie->PATPASS_TITLE;
  $this->patpass_notify_url = Configuration::get('PATPASS_NOTIFYURL');
  $this->patpass_postback_url = Configuration::get('PATPASS_POSTBACKURL');

}



}
