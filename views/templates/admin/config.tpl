<body onload="">
	<div>
		<link rel="stylesheet" href="../modules/patpass/views/css/bootstrap.min.css">
		<link href="../modules/patpass/views/css/bootstrap-switch.css" rel="stylesheet">
		<link href="../modules/patpass/views/css/tbk.css" rel="stylesheet">
		<script src="https://unpkg.com/bootstrap-switch@3.3.4/dist/js/bootstrap-switch.js"></script>
		<input type="hidden" id="urlbase" name="urlbase" value="{$this_path_ssl}">

		<h2>{l s='Pago electronico con Tarjetas de Crédito a traves de PatPass by Webpay' mod='patpass'}</h2>

		<button  class ="tbk_danger_btn" data-toggle="modal" data-target="#tb_modal">Informaci&#243;n</button>

		<hr>

		<fieldset>
			<form action="{$post_url|escape:'htmlall':'UTF-8'}" method="post" style="clear: both; margin-top: 10px;">
				<div class="row">
					<div class="col-md-12">
						<h3 class="tbk_title_h3">> {l s='Configuración' mod='patpass'}</h3>
						{if isset($errors.merchantERR)}
							<div class="error">
								<p>{$errors.merchantERR|escape:'htmlall':'UTF-8'}</p>
							</div>
						{/if}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="storeID">- {l s='Código de Comercio' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<input type="text" size="90" id="storeID" name="storeID" value="{$data_storeid|escape:'htmlall':'UTF-8'}"/>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="commerceMail">- {l s='Correo de Comercio' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<input type="text" size="90" id="commerceMail" name="commerceMail" value="{$data_commercemail|escape:'htmlall':'UTF-8'}"/>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="secretCode">- {l s='Llave privada' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<textarea cols="90" rows="6" wrap="soft" placeholder="" name="secretCode" id="secretCode" value="{$data_secretcode|escape:'htmlall':'UTF-8'}">{$data_secretcode|escape:'htmlall':'UTF-8'}</textarea>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="certificate">- {l s='Certificado' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<textarea cols="90" rows="6" wrap="soft" id="certificate" name="certificate" value="{$data_certificate|escape:'htmlall':'UTF-8'}"/>{$data_certificate|escape:'htmlall':'UTF-8'}</textarea>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="certificateTransbank">- {l s='Certificado Transbank' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<textarea cols="90" rows="6" wrap="soft" id="certificateTransbank" name="certificateTransbank" value="{$data_certificatetransbank|escape:'htmlall':'UTF-8'}"/>{$data_certificatetransbank|escape:'htmlall':'UTF-8'}</textarea>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="expirationDay">- {l s='Día de Cobro' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<select name="expirationDay" id="expirationDay" default="0">
								<option value="0" {if $data_expirationday eq 0}selected{/if}>Día de la compra</option>
								<option value="1" {if $data_expirationday eq 1}selected{/if}>Primer día del mes</option>
								<option value="5" {if $data_expirationday eq 5}selected{/if}>Día 5</option>
								<option value="10" {if $data_expirationday eq 10}selected{/if}>Día 10</option>
								<option value="15" {if $data_expirationday eq 15}selected{/if}>Día 15</option>
								<option value="20" {if $data_expirationday eq 20}selected{/if}>Día 20</option>
								<option value="25" {if $data_expirationday eq 25}selected{/if}>Día 25</option>
								<option value="99" {if $data_expirationday eq 99}selected{/if}>Último día del mes</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="ufFlag">- {l s='Valor en UF' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<select name="ufFlag" id="ufFlag" default="false">
								<option value="true" {if $data_ufflag eq true or $data_ufflag eq "true"}selected{/if}>Habilitado</option>
								<option value="false" {if $data_ufflag eq false or $data_ufflag eq "false"}selected{/if}>Deshabilitado</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-2">
						<label for="ambient">- {l s='Ambiente' mod='patpass'}</label>
					</div>
					<div class="col-md-8">
						<div class="margin-form">
							<select name="ambient" id="ambiente" default="INTEGRACION">
								<option value="INTEGRACION" {if $data_ambient eq "INTEGRACION"}selected{/if}>Integracion</option>
								<option value="PRODUCCION" {if $data_ambient eq "PRODUCCION"}selected{/if}>Produccion</option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
						<div align="right">
							<button type="submit" value="1" id="patpass_updateSettings" name="patpass_updateSettings" class="btn btn-default pull-right">
								<i class="process-icon-save" value="{l s='Save Settings' mod='patpass'}"></i> Guardar
							</button>
						</div>
					</div>
				</div>
			</form>
		</fieldset>

		<div class="modal" id="tb_modal">
			<div class="modal-dialog" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<ul class="nav nav-tabs">
							<li class="active" >
								<a data-toggle="tab" href="#info" class="tbk_tabs">{l s='Información' mod='patpass'}</a>
							</li>
							<li>
								<a data-toggle="tab" href="#php_info" class="tbk_tabs">{l s='PHP info' mod='patpass'}</a>
							</li>
							<li>
								<a data-toggle="tab" href="#logs" class="tbk_tabs">{l s='logs' mod='patpass'}</a>
							</li>
						</ul>
					</div>
					<div class="modal-body">
						<div class="tab-content">
							<div id="info" class="tab-pane in active">
								<fieldset>
									<h3 class="tbk_title_h3">{l s='Informe PDF' mod='patpass'}</h3>
									<form method="post" action="../modules/patpass/createpdf.php" target="_blank">
										<input type="hidden" name="ambient" value="{$data_ambient}">
										<input type="hidden" name="storeID" value="{$data_storeid}">
										<input type="hidden" name="certificate" value="{$data_certificate}">
										<input type="hidden" name="secretCode" value="{$data_secretcode}">
										<input type="hidden" name="certificateTransbank" value="{$data_certificatetransbank}">
										<input type="hidden" name="document" value="report">
										<input type="hidden" name="commerceMail" value="{$data_commercemail}">
										<input type="hidden" name="ufFlag" value="{$data_ufflag}">
										<input type="hidden" name="expirationDay" value="{$data_expirationday}">
										<button type="submit" class="btn">
											<i class="icon-file-text" value="{l s='genera pdf' mod='patpass'}"></i> Crear PDF
										</button>
									</form>
									<br>
								</fieldset>

								<br>
								<h3 class="tbk_title_h3">{l s='Información de Plugin / Ambiente' mod='patpass'}</h3>
								<table class="tbk_table_info">
									<tr>
										<td>
											<div title="Nombre del E-commerce instalado en el servidor" class="label label-info">?</div>
											<strong>{l s='Software E-commerce' mod='patpass'}:</strong>
										</td>
										<td class="tbk_table_td">
											{$ecommerce}
										</td>
									</tr>
									<tr>
										<td>
											<div title="Versión de {$ecommerce} instalada en el servidor" class="label label-info">?</div>
											<strong>{l s='Version E-commerce' mod='patpass'}:</strong>
										</td>
										<td class="tbk_table_td">
											{$ecommerce_version}
										</td>
									</tr>
									<tr>
										<td>
											<div title="Versión del plugin PatPass para {$ecommerce} instalada actualmente" class="label label-info">?</div>
											<strong>{l s='current_plugin_version' mod='patpass'}:</strong>
										</td>
										<td class="tbk_table_td">
											{$current_plugin_version}
										</td>
									</tr>
									<tr>
										<td>
											<div title="Última versión del plugin PatPass para {$ecommerce} disponible" class="label label-info">?</div>
											<strong>{l s='last_plugin_version' mod='patpass'}:</strong>
										</td>
										<td class="tbk_table_td">
											{$last_plugin_version}
										</td>
									</tr>
								</table>
								<br>
								<h3 class="tbk_title_h3">{l s='Validación certificados' mod='patpass'}</h3>
								<h4 class="tbk_table_title">{l s='Consistencias' mod='patpass'}</h4>
								<table class="tbk_table_info">
									<tr>
										<td>
											<div title="Informa si las llaves ingresadas por el usuario corresponden al certificado entregado por Transbank" class="label label-info">?</div>
											<strong>{l s='Consistencias con llaves' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											<span class="label {if $cert_vs_private eq 'OK'}label-success2{else}label-danger2{/if}">{$cert_vs_private}</span>
										</td>
									</tr>
									<tr>
										<td>
											<div title="Informa si el código de comercio ingresado por el usuario corresponde al certificado entregado por Transbank" class="label label-info">?</div>
											<strong>{l s='Validación Código de comercio' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											<span class="label {if $commerce_code_validate eq 'OK'}label-success2{else}label-danger2{/if}">{$commerce_code_validate}</span>
										</td>
									</tr>
								</table>
								<hr>
								<h4 class="tbk_table_title">{l s='cert_info' mod='patpass'}</h4>
								<table class="tbk_table_info">
									<tr>
										<td>
											<div title="CN (common name) dentro del certificado, en este caso corresponde al código de comercio emitido por Transbank" class="label label-info">?</div>
											<strong>{l s='Código de Comercio Válido' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">{$subject_commerce_code}</td>
									</tr>
									<tr>
										<td>
											<div title="Versión del certificado emitido por Transbank" class="label label-info">?</div>
											<strong>{l s='Versión certificado' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											{$cert_version}
										</td>
									</tr>
									<tr>
										<td>
											<div title="Informa si el certificado está vigente actualmente" class="label label-info">?</div>
											<strong>{l s='Vigencia' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											<span class="label {if $cert_is_valid eq 'OK'}label-success2{else}label-danger2{/if}">{$cert_is_valid}</span>
										</td>
									</tr>
									<tr>
										<td>
											<div title="Fecha desde la cual el certificado es válido" class="label label-info">?</div>
											<strong>{l s='valid_from' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											{$valid_from}
										</td>
									</tr>
									<tr>
										<td>
											<div title="Fecha hasta la cual el certificado es válido" class="label label-info">?</div>
											<strong>{l s='valid_to' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											{$valid_to}
										</td>
									</tr>
								</table>
								<br>
								<h3 class="tbk_title_h3">{l s='php_extensions_status' mod='patpass'}</h3>
								<h4 class="tbk_table_title">{l s='Información Principal' mod='patpass'}</h4>
								<table class="tbk_table_info">
									<tr>
										<td>
											<div title="Descripción del Servidor Web instalado" class="label label-info">?</div>
											<strong>{l s='Software Servidor' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											{$server_version}
										</td>
									</tr>
								</table>
								<hr>
								<h4 class="tbk_table_title">{l s='PHP' mod='patpass'}</h4>
								<table class="tbk_table_info">
									<tr>
										<td>
											<div title="Informa si la versión de PHP instalada en el servidor es compatible con el plugin de PatPass" class="label label-info">?</div>
											<strong>{l s='Estado de PHP' mod='patpass'}</strong>
										</td>
										<td class="tbk_table_td">
											<span class="label {if $php_status eq 'OK'}label-success2{else}label-danger2{/if}">{$php_status}</span>
										</td>
									</tr>
									<tr>
										<td>
											<div title="Versión de PHP instalada en el servidor" class="label label-info">?</div>
											<strong>{l s='version' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td">
											{$php_version}
										</td>
									</tr>
								</table>
								<hr>
								<h4 class="tbk_table_title">{l s='Extensiones PHP requeridas' mod='patpass'}</h4>
								<table class="table table-responsive table-striped">
									<tr>
										<th>
											{l s='Extensión' mod='patpass'}
										</th>
										<th>
											{l s='Estado' mod='patpass'}
										</th>
										<th class="tbk_table_td">
											{l s='Versión' mod='patpass'}
										</th>
									</tr>
									<tr>
										<td style="font-weight:bold">
											{l s='openssl' mod='patpass'}
										</td>
										<td>
											<span class="label {if $openssl_status eq 'OK'}label-success2{else}label-danger2{/if}">{$openssl_status}</span>
										</td>
										<td class="tbk_table_td">
											{$openssl_version}
										</td>
									</tr>
									<tr>
										<td style="font-weight:bold">
											{l s='SimpleXml' mod='patpass'}
										</td>
										<td>
											<span class="label {if $openssl_status eq 'OK'}label-success2{else}label-danger2{/if}">{$SimpleXML_status}</span>
										</td>
										<td class="tbk_table_td">
											{$SimpleXML_version}
										</td>
									</tr>
									<tr>
										<td style="font-weight:bold">
											{l s='soap' mod='patpass'}
										</td>
										<td>
											<span class="label {if $openssl_status eq 'OK'}label-success2{else}label-danger2{/if}">{$soap_status}</span>
										</td>
										<td class="tbk_table_td">
											{$soap_version}
										</td>
									</tr>
									<tr>
										<td style="font-weight:bold">
											{l s='mcrypt' mod='patpass'}
										</td>
										<td>
											<span class="label {if $openssl_status eq 'OK'}label-success2{else}label-danger2{/if}">{$mcrypt_status}</span>
										</td>
										<td class="tbk_table_td">
											{$mcrypt_version}
										</td>
									</tr>
									<tr>
										<td style="font-weight:bold">
											{l s='dom' mod='patpass'}
										</td>
										<td>
											<span class="label {if $openssl_status eq 'OK'}label-success2{else}label-danger2{/if}">{$dom_status}</span>
										</td>
										<td class="tbk_table_td">
											{$dom_version}
										</td>
									</tr>
								</table>
								<br>
								<h3 class="tbk_title_h3">{l s='Validación Transacción' mod='patpass'}</h3>
								<table class="tbk_table_info">
									<tr>
										<td>
											<h4 class="tbk_table_title">{l s='Petición a Transbank' mod='patpass'}</h4>
										</td>
										<td>
											<button id="healthCheck" class="btn" style="min-width: 150px;">Verificar Conexi&#243;n...</button>
										</td>
									</tr>
								</table>
								<hr>
								<h4 class="tbk_table_title">{l s='Respuesta de Transbank' mod='patpass'}</h4>
								<table class="tbk_table_info">
									<tr>
										<td height="42">
											<div title="Informa el estado de la comunicación con Transbank mediante método init_transaction" class="label label-info">?</div>
											<strong>{l s='status' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_td" height="42">
											<span id="result_init"><pre>---</pre></span>
										</td>
									</tr>
									<tr>
										<td height="42">
											<div title="URL entregada por Transbank para realizar la transacción" class="label label-info">?</div>
											<strong>{l s='URL' mod='patpass'}: </strong>
										</td>
										<td class="tbk_table_trans" height="42">
											<pre><span id="url_init">---</span></pre>
										</td>
									</tr>
									<tr>
										<td height="42">
											<div title="Token entregada por Transbank para realizar la transacción" class="label label-info">?</div>
											<strong>{l s='Token' mod='patpass'}: </strong>
										</td height="42">
										<td class="tbk_table_trans">
											<pre><span id="token_init">---</span></pre>
										</td>
									</tr>
								</table>
							</div>

							<div id="php_info" class="tab-pane">
								<fieldset>
									<h3 class="tbk_title_h3">{l s='Informe PHP info ' mod='patpass'}</h3>
									<form method="post" action="../modules/patpass/createpdf.php" target="_blank">
										<input type="hidden" name="ambient" value="{$data_ambient}">
										<input type="hidden" name="storeID" value="{$data_storeid}">
										<input type="hidden" name="certificate" value="{$data_certificate}">
										<input type="hidden" name="secretCode" value="{$data_secretcode}">
										<input type="hidden" name="certificateTransbank" value="{$data_certificatetransbank}">
										<input type="hidden" name="document" value="php_info">
										<input type="hidden" name="commerceMail" value="{$data_commercemail}">
										<input type="hidden" name="ufFlag" value="{$data_ufflag}">
										<input type="hidden" name="expirationDay" value="{$data_expirationday}">
										<button type="submit" class="btn">
											<i class="icon-file-text" value="{l s='Crear PHP info' mod='patpass'}"></i> Crear PHP info
										</button>
									</form>
								</fieldset>
								<br>
								<fieldset>
									<br>
									<h3 class="tbk_title_h3">{l s='php_info' mod='patpass'}</h3>
									<span style="font-size: 10px; font-family:monospace; display: block; background: white;overflow: hidden; width: 90%;" >{$php_info}</span><br>
								</fieldset>
							</div>

							<div id="logs" class="tab-pane">
								<fieldset>
									<h3 class="tbk_title_h3">{l s='Configuración' mod='patpass'}</h3>
									<form method="post" action ="../modules/patpass/call_loghandler.php" id="log_form" target= "_blank">
										<table class="tbk_table_info">
											<tr>
												<td>
													<div title="Al activar esta opción se habilita que se guarden los datos de cada compra mediante PatPass" class="label label-info">?</div>
													<strong>{l s="Activar Registro:" mod='patpass'} </strong>
												</td>
												<td class="tbk_table_td">
													{if $lockfile}
														<input type="checkbox" id="action_check" name="action_check" checked data-size="small" value="activate">
													{else}
														<input type="checkbox" id="action_check" name="action_check" data-size="small" state="false">
													{/if}
												</td>
											</tr>
										</table>
										<script> $("[name='action_check']").bootstrapSwitch();</script>

										<table class="tbk_table_info">
											<tr>
												<td>
													<div title="Cantidad de días que se conservan los datos de cada compra mediante PatPass" class="label label-info">?</div>
													<strong>{l s="Cantidad de Dias a Registrar" mod='patpass'}: </strong>
												</td>
												<td class="tbk_table_td">
													<input id="days" name="days" type="number" min="1" max="30" value="{$log_days}">{l s=" días"}
												</td>
											</tr>
											<tr>
												<td>
													<div title="Peso máximo (en Megabytes) de cada archivo que guarda los datos de las compras mediante PatPass" class="label label-info">?</div>
													<strong>{l s="Peso máximo de Registros" mod='patpass'}:  </strong>
												</td>
												<td class="tbk_table_td">
													<select style="width: 100px; display: initial;" id="size" name="size">
														{for $c=1 to 10}
															<option value="{$c}" {if $c eq $log_size}selected{/if}>{$c}</option>
														{/for}
													</select> {l s="Mb" mod='patpass'}
												</td>
											</tr>
										</table>
										<div class="tbk_btn tbk_danger_btn" onclick="swap_action()">{l s='Actualizar Parametros' mod='patpass'}</div>
									</form>
									<hr style="border-top: 1px solid #8a8a8a;">
									<h3 class="tbk_title_h3">{l s='Información de Registros' mod='patpass'}</h3>
									<table class="tbk_table_info">
										<tr>
											<td>
												<div title="Informa si actualmente se guarda la información de cada compra mediante PatPass" class="label label-info">?</div>
												<strong>{l s="Estado de Registros" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												<span id="action_txt" class="label label-success2">{l s='Registro-activado' mod='patpass' }</span><br>
											</td>
										</tr>
										<tr>
											<td>
												<div title="Carpeta en el servidor en donde se guardan los archivos con la informacón de cada compra mediante PatPass" class="label label-info">?</div>
												<strong>{l s="Directorio de registros" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												{$log_dir}
											</td>
										</tr>
										<tr>
											<td>
												<div title="Cantidad de archivos que guardan la información de cada compra mediante PatPass" class="label label-info">?</div>
												<strong>{l s="Cantidad de Registros en Directorio" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												{$logs_count}
											</td>
										</tr>
										<tr>
											<td>
												<div title="Lista los archivos archivos que guardan la información de cada compra mediante PatPass" class="label label-info">?</div>
												<strong>{l s="Listado de Registros Disponibles" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												<ul style="font-size:0.8em;">
													{foreach from=$logs_list item=index}
														<li>{$index}</li>
													{/foreach}
												</ul>
											</td>
										</tr>
									</table>
									<hr style="border-top: 1px solid #8a8a8a;">
									<h3 class="tbk_title_h3">{l s='Últimos Registros' mod='patpass'}</h3>
									<table class="tbk_table_info">
										<tr>
											<td>
												<div title="Nombre del útimo archivo de registro creado" class="label label-info">?</div>
												<strong>{l s="Último Documento" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												{$log_file}
											</td>
										</tr>
										<tr>
											<td>
												<div title="Peso del último archivo de registro creado" class="label label-info">?</div>
												<strong>{l s="Peso del Documento" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												{$log_weight}
											</td>
										</tr>
										<tr>
											<td>
												<div title="Cantidad de líneas que posee el último archivo de registro creado" class="label label-info">?</div>
												<strong>{l s="Cantidad de Líneas" mod='patpass'}: </strong>
											</td>
											<td class="tbk_table_td">
												{$log_regs_lines}
											</td>
										</tr>
									</table>
									<br>
									<pre>
										<span style="font-size: 10px; font-family:monospace; display: block; background: white;width: fit-content;" >{$logs}</span>
									</pre>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">
							Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>

		<script>
			// Valores por Defecto de Integracion
			var data_storeid_init = "{$data_storeid_init}";
			var data_commercemail_init = "{$data_commercemail_init}";
			var data_secretcode_init = "{$data_secretcode_init}".replace(/<br\s*\/?>/mg,"\n");
			var data_certificate_init = "{$data_certificate_init}".replace(/<br\s*\/?>/mg,"\n");
			var data_certificatetransbank_init = "{$data_certificatetransbank_init}".replace(/<br\s*\/?>/mg,"\n");
			var data_ufflag_init = "{$data_ufflag_init}";
			var data_expirationday_init = "{$data_expirationday_init}";

			// Valores Almacenados para Produccion
			var data_storeid = $('#storeID').val();
			var data_commercemail = $('#commerceMail').val();
			var data_secretcode = $('#secretCode').val();
			var data_certificate = $('#certificate').val();
			var data_certificatetransbank = $('#certificateTransbank').val();
			var data_ufflag = $('#ufFlag').prop('checked');
			var data_expirationday = $('#expirationDay').val();

			// Intervalo puntos
			var interval = null;

			function swap_action(){
				if (document.getElementById("action_check").checked){
					document.getElementById('action_txt').innerHTML = 'Registro activado';
					$('#action_txt').removeClass("label-warning").addClass("label-success2");
				}
				else{
					document.getElementById('action_txt').innerHTML = 'Registro desactivado';
					$('#action_txt').removeClass("label-success2").addClass("label-warning");
				}
				document.getElementById("log_form").submit();
			}
		</script>
		<script type="text/javascript" src="../../../..{$url_js}"></script>
		{literal}
		<script type="text/javascript">
		    $(function(){
		    	// Cambio de Ambiente
		    	$('#ambiente').on('change',function(){
		    		if(this.value=='INTEGRACION'){
		    			$('#storeID').val(data_storeid_init);
						$('#commerceMail').val(data_commercemail_init);
						$('#secretCode').val(data_secretcode_init);
						$('#certificate').val(data_certificate_init);
						$('#certificateTransbank').val(data_certificatetransbank_init);
						$('#expirationDay').val(data_expirationday_init);
						$('#ufFlag').val(data_ufflag_init);
		    		}
		    		else{
		    			$('#storeID').val(data_storeid);
						$('#commerceMail').val(data_commercemail);
						$('#secretCode').val(data_secretcode);
						$('#certificate').val(data_certificate);
						$('#certificateTransbank').val(data_certificatetransbank);
						$('#expirationDay').val(data_expirationday);
						$('#ufFlag').val(data_ufflag);
		    		}
		    	});
		    	// Realizar HealthCheck
		    	$('#healthCheck').on('click',function(){
		    		interval = setInterval(type,100);
					$("#healthCheck").html("Verificando<span id='dots'></span>").prop('disabled', true);
					$("#result_init").removeClass('label').removeClass('label-success2').removeClass('label-danger2');
					$("#result_init").html('<pre>---</pre>');
					$("#url_init").html('---');
					$("#token_init").html('---');			
					
					var data = {
						"TYPE" : "checkInit",
						"MODE" : $("input[name='ambient']").val(),
						"COMMERCE_CODE" : $("input[name='storeID']").val(),
						"PUBLIC_CERT" : $("input[name='certificate']").val(),
						"PRIVATE_KEY" : $("input[name='secretCode']").val(),
						"PATPASS_CERT" : $("input[name='certificateTransbank']").val(),
						"PATPASS_MAIL" : $("input[name='commerceMail']").val(),
						"PATPASS_UF" : $("#ufFlag").val(),
						"PATPASS_DAY" : $("input[name='expirationDay']").val(),
					};
					
					$.post('../modules/patpass/controllers/admin/init_healthcheck.php',data,function(response){
						clearInterval(interval);
						$("#healthCheck").text("Verificar Conexión...").prop('disabled', false);

						if(response.success){
							if(response.msg.status.string == "OK")
							{
								$("#result_init").addClass('label').addClass('label-success2');
								$("#result_init").html('OK');
								$("#url_init").html(response.msg.response.url);
								$("#token_init").html(response.msg.response.token_ws);
							}
							else
							{	
								$("#result_init").addClass('label').addClass('label-danger2');
								$("#result_init").html('ERROR');
								$("#url_init").html('---');
								$("#token_init").html('---');
							}
						}
						else{
							$("#result_init").addClass('label').addClass('label-danger2');
							$("#result_init").html('ERROR');
							$("#url_init").html('---');
							$("#token_init").html('---');
						}	
					},'json');
				});
				// Puntitos
				function type(){
					if(dots < 3){
						$('#dots').append('.');
						dots++;
					}
					else{
						$('#dots').html('');
						dots = 0;
					}
				}
		    });
		</script>
		{/literal}
	</div>
</body>