{extends file='page.tpl'}
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}
{block name="content"}
{if ({$validation} == '0')}
	<form class="dataPayment">
		<div class="box cheque-box">
			<h2 class="page-subheading">Solicitud de Informaci&#243;n</h2>
			<p>Para finalizar la Suscripci&#243;n a trav&#233;s de <b>PatPass by WebPay</b> se necesitan algunos datos extras.</p>
			<input type="hidden" id="urlbase" name="urlbase" value="{$this_path_ssl}">
			<!-- Nombres -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">Nombres</label>
				<div class="col-md-6">
					<input class="form-control" name="firstname" id="firstname" type="text" value="{$name}" placeholder="Nombres..." required>
				</div>
				<div class="col-md-3 form-control-comment firstname-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
			<!-- Apellido Paterno -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">Apellido Paterno</label>
				<div class="col-md-6">
					<input class="form-control" name="lastname1" id="lastname1" type="text" value="{$lastname}" placeholder="Apellido Paterno..." required>
				</div>
				<div class="col-md-3 form-control-comment lastname1-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
			<!-- Apellido Materno -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">Apellido Materno</label>
				<div class="col-md-6">
					<input class="form-control" name="lastname2" id="lastname2" type="text" value="" placeholder="Apellido Materno..." required>
				</div>
				<div class="col-md-3 form-control-comment lastname2-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
			<!-- RUT -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">RUT</label>
				<div class="col-md-6">
					<input class="form-control" name="rut" id="rut" type="text" onkeyup="formateaRut(this)" placeholder="11.111.111-1" required>
				</div>
				<div class="col-md-3 form-control-comment rut-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
			<!-- Correo -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">Correo</label>
				<div class="col-md-6">
					<input class="form-control" name="email" id="email" type="email" value="{$mail}" placeholder="correo@ejemplo.cl" required>
				</div>
				<div class="col-md-3 form-control-comment email-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
			<!-- Telefono -->
			<div class="form-group row ">
				<label class="col-md-3 form-control-label required">Tel&#233;fono</label>
				<div class="col-md-6">
					<input class="form-control" name="tel" id="tel" type="text" onkeyup="soloNumeros(this)" placeholder="56912345678" maxlength="11" required>
				</div>
				<div class="col-md-3 form-control-comment tel-comment" style="max-height: 38px;margin-top: -7px;"></div>
			</div>
		</div>
		<p>&#32;</p>
		<div class="row">
			<div class="col-md-1">
				<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="btn btn-default"><i class="icon-chevron-left"></i>{l s='Volver' mod='patpass'}</a>
			</div>
			<div class="col-md-2">
				<button id="continueBtn" type="button" class="btn btn-primary" >
					<span>Continuar</span>
				</button>
			</div>
			<div id="loading" class="col-md-1" style="display:none">
				<img src="../../../img/loader.gif"/>
			</div>
		</div>
		<p>&#32;</p><p>&#32;</p>
	</form>
	<!-- Confirmacion Pago -->
	<form method="post" action="" id="confirmPayment" class="confirmPayment" style="display:none">
		<input type="hidden" name="token_ws" id="token_ws" value="" />
		<div class="box cheque-box">
			<h3 class="page-subheading">Pago por PatPass by WebPay</h3>
			<p>Se realizara la Suscripci&#243;n a traves de <b>PatPass by WebPay</b> por un total mensual de ${$total}.</p>
		</div>
		<p class="cart_navigation clearfix" id="cart_navigation">
			<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="btn btn-default"><i class="icon-chevron-left"></i>{l s='Volver' mod='patpass'}</a>
			<button type="submit" class="btn btn-primary">
				<span>Realizar Pago</span>
			</button>
		</p>
	</form>
	<!-- Error -->
	<form id="failPayment" style="display:none">
		<p id="msgerror" class="alert alert-danger">{$errmsg}</p>   
		<p class="cart_navigation clearfix" id="cart_navigation">
			<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button-exclusive btn btn-default"><i class="icon-chevron-left"></i>{l s='Volver' mod='patpass'}</a>
		</p>
	</form>

	<script type="text/javascript" src="../../../..{$url_js}"></script>
	{literal}
	<script type="text/javascript">

	    $(function(){

	    	$('#continueBtn').on('click',function(){
	    		$('.btn').attr('disabled',true);
	    		$('#loading').show();
	    		if(validaDatos()){
		    		$.ajax({
				        type: 'POST',
				        dataType: 'json',
				        url: $('#urlbase').val()+'controllers/front/init.php',
				        data: 'firstname='+$('#firstname').val()+
				        	  '&lastname1='+$('#lastname1').val()+
				        	  '&lastname2='+$('#lastname2').val()+
				        	  '&rut='+$('#rut').val()+
				        	  '&email='+$('#email').val()+
				        	  '&tel='+$('#tel').val()
				    }).done(function(data){
				    	//console.log('Success!');
				    	//console.log(JSON.stringify(data));
				    	if(data['error']==0){
				    		$('#loading').hide();
				    		$('.dataPayment').hide();
					    	$('.confirmPayment').fadeIn(300);
					    	$('.btn').attr('disabled',false);
					    	$('#confirmPayment').attr('action', data['url_ws']);
					    	$('#token_ws').val(data['token_ws']);
				    	}
				    	else if(data['error']==1){
				    		$('#loading').hide();
					        $('#failPayment').fadeIn(300);
					        $('.dataPayment').hide();
					        $('.confirmPayment').hide();
					        $('.btn').attr('disabled',false);
					        $('#confirmPayment').attr('action', '');
					    	$('#token_ws').val('');
					    	$('#msgerror').html(data['msg']);
				    	}
				    	else{
				    		$('#loading').hide();
					        $('#failPayment').fadeIn(300);
					        $('.dataPayment').hide();
					        $('.confirmPayment').hide();
					        $('.btn').attr('disabled',false);
					        $('#confirmPayment').attr('action', '');
					    	$('#token_ws').val('');
					    	$('#msgerror').html(data['msg']);
				    	}
				    	
				    }).fail(function(data){
				    	//console.log('Error!');
				        //console.log(JSON.stringify(data));
				        $('#loading').hide();
				        $('.dataPayment').fadeIn(300);
				        $('.confirmPayment').hide();
				        $('.btn').attr('disabled',false);
				        $('#confirmPayment').attr('action', '');
				    	$('#token_ws').val('');
				    });
	    		}
	    		else{
	    			$('#loading').hide();
	    			$('.btn').attr('disabled',false);
	    			//console.log('Faltan Datos');
	    		}
	    		
	    	});

	    	function validaDatos(){
	    		// Limpieza
	    		$('.firstname-comment').html('');
	    		$('#firstname').css('border-color','');
	    		$('.lastname1-comment').html('');
	    		$('#lastname1').css('border-color','');
	    		$('.lastname2-comment').html('');
	    		$('#lastname2').css('border-color','');
	    		$('.rut-comment').html('');
	    		$('#rut').css('border-color','');
	    		$('.email-comment').html('');
	    		$('#email').css('border-color','');
	    		$('.tel-comment').html('');
	    		$('#tel').css('border-color','');
	    		// Validación por Campo
	    		if($('#firstname').val()==null || ($('#firstname').val()).trim()==''){
	    			//console.log('Nombre vacio');
	    			$('.firstname-comment').html('<p class="alert alert-danger" style="max-height: 38px">* Nombre Inv&#225;lido *</p>');
	    			$('#firstname').css('border-color','red');
	    			return false;
	    		}
	    		if($('#lastname1').val()==null || ($('#lastname1').val()).trim()==''){
	    			//console.log('Apellido Paterno vacio');
	    			$('.lastname1-comment').html('<p class="alert alert-danger" style="max-height: 38px">* Apellido Paterno Inv&#225;lido *</p>');
	    			$('#lastname1').css('border-color','red');
	    			return false;
	    		}
	    		if($('#lastname2').val()==null || ($('#lastname2').val()).trim()==''){
	    			//console.log('Apellido Materno vacio');
	    			$('.lastname2-comment').html('<p class="alert alert-danger" style="max-height: 38px">* Apellido Materno Inv&#225;lido *</p>');
	    			$('#lastname2').css('border-color','red');
	    			return false;
	    		}
	    		if(!checkRut($('#rut').val())){
	    			//console.log('RUT vacio');
	    			$('.rut-comment').html('<p class="alert alert-danger" style="max-height: 38px">* RUT Inv&#225;lido *</p>');
	    			$('#rut').css('border-color','red');
	    			return false;
	    		}
	    		if($('#email').val()==null || ($('#email').val()).trim()==''){
	    			//console.log('Email vacio');
	    			$('.email-comment').html('<p class="alert alert-danger" style="max-height: 38px">* Correo Inv&#225;lido *</p>');
	    			$('#email').css('border-color','red');
	    			return false;
	    		}
	    		if($('#tel').val()==null || ($('#tel').val()).trim()==''){
	    			//console.log('Telefono vacio');
	    			$('.tel-comment').html('<p class="alert alert-danger" style="max-height: 38px">* Tel&#233;fono Inv&#225;lido *</p>');
	    			$('#tel').css('border-color','red');
	    			return false;
	    		}
	    		return true;
	    	}

		});

		function formateaRut(rut){
			var actual = (rut.value).replace(/^0+/, "");
			if (actual != '' && actual.length > 1) {
				var sinPuntos = actual.replace(/\./g, "");
				var actualLimpio = sinPuntos.replace(/-/g, "");
				var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
				var rutPuntos = "";
				var i = 0;
				var j = 1;
				for (i = inicio.length - 1; i >= 0; i--){
					var letra = inicio.charAt(i);
					rutPuntos = letra + rutPuntos;
					if (j % 3 == 0 && j <= inicio.length - 1){
						rutPuntos = "." + rutPuntos;
					}
					j++;
				}
				var dv = actualLimpio.substring(actualLimpio.length - 1);
				rutPuntos = rutPuntos + "-" + dv;
			}
			if(typeof rutPuntos === "undefined"){
				$("#rut").val(rut.value);
				//checkRut(rut.value);
			}
			else{
				$("#rut").val(rutPuntos);
				//checkRut(rutPuntos);
			}
		}

		function checkRut(rut){
			var valor = rut.replace(/\./g,'');
			valor = valor.replace('-','');
			cuerpo = valor.slice(0,-1);
			dv = valor.slice(-1).toUpperCase();
			rut = cuerpo + '-'+ dv
			if(cuerpo.length < 7){
				//console.log("RUT Incompleto");
				return false;
			}
			suma = 0;
			multiplo = 2;
			for(i=1;i<=cuerpo.length;i++){
				index = multiplo * valor.charAt(cuerpo.length - i);
				suma = suma + index;
				if(multiplo < 7){
					multiplo = multiplo + 1;
				}
				else{
					multiplo = 2;
				}
			}
			dvEsperado = 11 - (suma % 11);
			dv = (dv == 'K')?10:dv;
			dv = (dv == 0)?11:dv;
			if(dvEsperado != dv){
				//console.log("RUT Inválido");
				return false;
			}
			//console.log('RUT CORRECTO');
			return true;
		}

		function soloNumeros(tel){
			tel.value = tel.value.replace(/[^0-9\.]/g,'');
			$("#tel").val(tel.value);
		}

	</script>
	{/literal}
{else}
	{if ({$validation} == '2')}
	<!-- Servicios en el Carro Superior al Permitido -->
	<p class="alert alert-danger">{$errmsg}</p>   
	<p class="cart_navigation clearfix" id="cart_navigation">
		<a href="{$link->getPageLink('cart', true, NULL, "action=show")|escape:'html'}" class="button-exclusive btn btn-default"><i class="icon-chevron-left"></i>{l s='Volver al Carro de Compras' mod='patpass'}</a>
	</p>
	{else}
	<p class="alert alert-danger">{$errmsg}</p>   
	<p class="cart_navigation clearfix" id="cart_navigation">
		<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}" class="button-exclusive btn btn-default"><i class="icon-chevron-left"></i>{l s='Volver' mod='patpass'}</a>
	</p>
	{/if}
{/if}
{/block}