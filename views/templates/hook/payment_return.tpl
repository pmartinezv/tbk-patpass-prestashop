{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if ($PATPASS_TX_ANULADA == "SI")}
  <p class="alert alert-danger">La Transaccion fue Anulada por el Cliente.</p>   
{else}
{if ($PATPASS_RESULT_CODE == 0)}
  <p class="alert alert-success">{l s='Su pedido está completo.'  mod='patpass'}</p>
  <div class="box order-confirmation">
  	<h3 class="page-subheading">Detalles del pago :</h3>
    <hr>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Respuesta de la Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_RESULT_DESC}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Tarjeta de Cr&#233;dito:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">**********{$PATPASS_VOUCHER_NROTARJETA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Fecha de Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_TXDATE_FECHA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Hora de Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_TXDATE_HORA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Monto Compra:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">${$PATPASS_VOUCHER_TOTALPAGO}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Orden de Compra:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_ORDENCOMPRA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>C&#243;digo de Autorizaci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_AUTCODE}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Tipo de Pago:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_TIPOPAGO}</div>
      <div class="col-md-6"></div>
    </div>
    <hr>
                  <!--<br />Tipo de Cuotas :  {$PATPASS_VOUCHER_TIPOCUOTAS}
                  <br />Numero de cuotas :  {$PATPASS_VOUCHER_NROCUOTAS}-->
  </div>
{else}
  <p class="alert alert-danger">Ha ocurrido un error con su pago. </p>  
  <div class="box order-confirmation">
   	<h3 class="page-subheading">Detalles del pago :</h3>
    <hr>
   	<div class="row">
      <div class="col-md-3">&#32;&#32;<i>Respuesta de la Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_RESULT_DESC}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Orden de Compra:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_ORDENCOMPRA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Fecha de Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_TXDATE_FECHA}</div>
      <div class="col-md-6"></div>
    </div>
    <div class="row">
      <div class="col-md-3">&#32;&#32;<i>Hora de Transacci&#243;n:</i></div>
      <div class="col-md-3" style="color: #7a7a7a;">{$PATPASS_VOUCHER_TXDATE_HORA}</div>
      <div class="col-md-6"></div>
    </div>
    <hr>
  </div>
{/if}
{/if}
